package edu.ncc.nest.nestapp;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.PopupMenu;
import android.widget.Toast;

public class Donate extends AppCompatActivity implements PopupMenu.OnMenuItemClickListener {

    private PopupMenu popper;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_donate);
    }

    public void showPopup(View v)
    {
        popper = new PopupMenu(this, v);
        popper.setOnMenuItemClickListener(this);
        if(v.getId() == R.id.testBtn2 || v.getId() == R.id.testBtn3)
        {
            popper.inflate(R.menu.popup_menu_2);
        }
        else
        {
            popper.inflate(R.menu.popup_menu);
        }
        popper.show();
    }

    @Override
    public boolean onMenuItemClick(MenuItem item) {
        switch (item.getItemId())
        {
            case R.id.category1:
                Toast.makeText(this, "Category 1 clicked", Toast.LENGTH_SHORT).show();
                return true;
            case R.id.category2:
                Toast.makeText(this, "Category 2 clicked", Toast.LENGTH_SHORT).show();
                return true;
            case R.id.category3:
                Toast.makeText(this, "Category 3 clicked", Toast.LENGTH_SHORT).show();
                return true;
            case R.id.category4:
                Toast.makeText(this, "Category 4 clicked", Toast.LENGTH_SHORT).show();
                return true;
            case R.id.category5:
                Toast.makeText(this, "Category 5 clicked", Toast.LENGTH_SHORT).show();
                popper.inflate(R.menu.popup_menu_3);
                popper.show();
                switch (item.getItemId())
                {
                    case R.id.category9:
                        Toast.makeText(this, "Category 9 clicked", Toast.LENGTH_SHORT).show();
                        return true;
                    case R.id.category10:
                        Toast.makeText(this, "Category 10 clicked", Toast.LENGTH_SHORT).show();
                        return true;
                    case R.id.category11:
                        Toast.makeText(this, "Category 11 clicked", Toast.LENGTH_SHORT).show();
                        return true;
                    case R.id.category12:
                        Toast.makeText(this, "Category 12 clicked", Toast.LENGTH_SHORT).show();
                        return true;
                }
                return true;
            case R.id.category6:
                Toast.makeText(this, "Category 6 clicked", Toast.LENGTH_SHORT).show();
                return true;
            case R.id.category7:
                Toast.makeText(this, "Category 7 clicked", Toast.LENGTH_SHORT).show();
                return true;
            case R.id.category8:
                Toast.makeText(this, "Category 8 clicked", Toast.LENGTH_SHORT).show();
                return true;
            default:
                return false;
        }
    }
}
